# 2.1.1
- Korrigiere Plugin-Namespace

# 2.1.0
- RC Kompatibilität
- Update der Sprachdateien

# 2.0.0
- EA 2 Kompatibilität

# 1.1.0
- Update der Sprachdateien

# 1.0.0
- Initielles Release des Sprachplugins für Spanisch für Shopware 6
