# 2.1.1
- Fix plugin namespace

# 2.1.0
- RC compatibility
- Update of language files

# 2.0.0
- EA 2 compatibility

# 1.1.0
- Update of language files

# 1.0.0
- Initial release of Spanish language plugin for Shopware 6
