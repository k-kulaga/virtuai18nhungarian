<?php declare(strict_types=1);
/*
 * (c) shopware AG <info@shopware.com>
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace VirtuaI18nHungarian\Resources\app\storefront\src\snippet;

use Shopware\Core\System\Snippet\Files\SnippetFileInterface;
use VirtuaI18nHungarian\VirtuaI18nHungarian;

class SnippetFile_hu_HU implements SnippetFileInterface
{
    public function getName(): string
    {
        return 'storefront.' . VirtuaI18nHungarian::VIRTUA_I18N_LOCALE_CODE;
    }

    public function getPath(): string
    {
        return __DIR__ . '/' . $this->getName() . '.json';
    }

    public function getIso(): string
    {
        return VirtuaI18nHungarian::VIRTUA_I18N_LOCALE_CODE;
    }

    public function getAuthor(): string
    {
        return 'Shopware Services';
    }

    public function isBase(): bool
    {
        return false;
    }
}
