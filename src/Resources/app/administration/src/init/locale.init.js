const locale = 'hu-HU';

if (Shopware.Locale.getByName(locale) === false) {
    Shopware.Locale.register(locale, {});
}